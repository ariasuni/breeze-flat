# Breeze Flat

[Breeze Light Flat][BLF] and [Breeze Dark Flat][BDF] are variants of Breeze, the default Plasma 5 theme. They use the opaque and not shadowed ressources, which are used as fallback when software rendering is unavailable.

Icons used on the KDE Store are a modification from Breeze [folder-activities icon][FAI], using colors of the [HIG].

Breeze Light Flat, Breeze Dark Flat, and the KDE store icons are released under the LGPLv3.

[BLF]: https://store.kde.org/p/1215691/
[BDF]: https://store.kde.org/p/1215690/
[FAI]: https://phabricator.kde.org/source/breeze-icons/browse/master/icons/places/22/folder-activities.svg
[HIG]: https://hig.kde.org/style/color/default.html
