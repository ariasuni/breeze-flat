#!/bin/sh

# If installing as a distro package, to be always immediately up-to-date

for orig_theme in breeze-light breeze-dark
do
  theme=${orig_theme}-flat
  ln -s /usr/share/plasma/desktoptheme/default/opaque/dialogs $theme
  ln -s /usr/share/plasma/desktoptheme/default/opaque/widgets $theme
  ln -s /usr/share/plasma/desktoptheme/$orig_theme/colors $theme
done
