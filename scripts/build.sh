#!/bin/sh

# If building for KDE Store, where symlinks won’t work well

if test -z $VERSION; then
  echo 'Needs a version number, e.g. `VERSION=1.0 scripts/build.sh`'
  exit
fi

mkdir -p build
for orig_theme in breeze-light breeze-dark
do
  theme=${orig_theme}-flat
  cp -r /usr/share/plasma/desktoptheme/default/opaque/dialogs $theme
  cp -r /usr/share/plasma/desktoptheme/default/opaque/widgets $theme
  cp -r /usr/share/plasma/desktoptheme/$orig_theme/colors $theme
  tar cfz "build/$theme-$VERSION.tar.gz" $theme
  rm -r $theme/dialogs $theme/widgets $theme/colors
done
